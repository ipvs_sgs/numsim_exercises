.. numsim documentation master file, created by
   sphinx-quickstart on Mon Jun 24 10:11:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Exercise Numerical Simulation
===============================

This website contains material for the exercises of the lecture Numerical Simulation (NumSim), including the tasks for the submissions. 

A time schedule exists in the `ILIAS course <https://ilias3.uni-stuttgart.de/goto_Uni_Stuttgart_crs_1737393.html>`_ 
for the lecture. 

If you want to contact us, you'll find us at the department `Simulation of Large Systems (Simulation großer Systeme, SGS) <https://www.ipvs.uni-stuttgart.de/abteilungen/sgs/>`_ 
at the `Institute for Parallel and Distributed Systems (IPVS) <https://www.ipvs.uni-stuttgart.de/index1.html>`_ at the University of Stuttgart.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   introduction
   exercise1/index
   exercise2/index
   exercise3b/index
   project/index
   
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
