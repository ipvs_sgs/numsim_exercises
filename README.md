The exercises text is available at https://numsim-exercises.readthedocs.io/ and built automatically.

To build the webpage, run

``` 
make html
```
