
Exercise 1
===============

The goal of this exercise is to program the **basic fluid solver** and to simulate the *lid driven cavity* scenario.

.. toctree::
   :maxdepth: 1
   :caption: Next pages to read
   
   general_remarks
   linux
   compiler
   build_system
   settings
   object_orientation
   architecture
   hints
   submission
   
