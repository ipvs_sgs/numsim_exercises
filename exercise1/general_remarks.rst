General remarks
-----------------------------

The tutorial starts with some general remarks regarding the organisation.

Previous knowledge
^^^^^^^^^^^^^^^^^^^^^
Since the audience is usually well mixed of Computer Scientists, Software Engineers, SimTechs, Mathematicians and Engineers, so is the previous knowledge and familiarity with the technical setup.
Therefore, some introductory tips may seem trivial to some participants, while others may have a harder time to get started. You may just skip over those sections that you already know well.

Language
^^^^^^^^^^^
As almost all literature and resources in research and programming, these tutorial pages will be in English, while the lecture and exercise will be held in German.
Your source code, i.e. variable names and comments should also be in English.

Workload
^^^^^^^^^^^^
The exercises for this lecture require some work during the semster. In turn, you'll be finished after the last submission and presentation as there will be no exam. 
The course has 6 LP that correspond to 30h of work, each.
According to the `module description in Campus <https://campus.uni-stuttgart.de/cusonline/WBMODHB.wbShowMHBReadOnly?pKnotenNr=90362&pOrgNr=986>`_, the work load is distributed as follows:

==================== ===========
Gesamtstunden	        180 
==================== ===========
Präsenzstunden	       42 
Eigenstudiumstunden	  138 
==================== ===========

`Präsenzstunden` means the time to attend the lectures and exercises. This is accounted for by :math:`2 \times 1.5 h = 3 h` per week for 14 weeks, i.e., :math:`42 h` in total. 
Then there is the "Abnahme" with :math:`4 \times \frac13 h` and the final project presentation. Dividing the remaining :math:`\approx 135 h` equally to the 4 exercises and project yields :math:`27 h` per submission.
In case of a two week interval between submissions, this gives you :math:`13.5 h` per week to work on the program, not including the time during the exercise.


Computer system
^^^^^^^^^^^^^^^^^^^
Development of the program should be performed on a Linux computer, because the discussed tools, e.g. the build system, may not exist or work differently on other systems.
Because we care about runtime performance, Linux is to be prefered over Windows. Nearly all supercomputers and compute servers run a Linux system.

If you only have a Windows installation so far, there are different options:

* Install a dual-boot setup with Linux. This is the most performant setup but requires some work. The linux distribution Ubuntu is recommended, you can use `Kubuntu <https://kubuntu.org/getkubuntu/>`_ (It has the KDE Plasma desktop which works and looks similar to the Windows Desktop.)
* Run Linux inside a virtual machine on Windows. We recommend to use `VirtualBox <https://www.virtualbox.org/>`_ with Ubuntu. This works well for development but execution times of the simulation program are about a factor of 10 slower.
* Do not install any Linux at all. We provide a user account on our servers and compute cluster that run Linux (currently Ubuntu 20.04). With Windows PowerShell you can log in to those machines using OpenSSH. Only programs in the terminal are possible, no program windows can be displayed. But this would be enough to work on the exercises.
  
  Similarly, also the `Windows Subsystem for Linux <https://docs.microsoft.com/en-us/windows/wsl/install-win10>`_ can be used to have Linux "natively" installed in Windows. No experience is documented so far how well this approach works.


User Account
^^^^^^^^^^^^^^

To get an IPVS account send an email to the organizer (david.schneider@ipvs.uni-stuttgart.de or jun.chen@ipvs.uni-stuttgart.de) containing the following information:

* full name
* matriculation number
* uni mail address (`@stud.uni-stuttgart.de`)
* course of study

After a few days of processing, you will receive an email with a username and an initial password. If there are any questions, please contact us. If you already had an account on any system of IPVS at an earlier time, this account will be reused without further notice.

Once you've got an account:

* The user name is: first 6 letters of surname (or all if surname has less than 6 letters) + first and last letter of prename, all in lower case letters.
* The next step is to change the initial password. Go to `<https://www.ipvs.uni-stuttgart.de/de/abteilungen/ifs/rechnerlabor/rechenbetrieb-studenten/passwort>`_ from within the Computer Science network and change your password (eduroam does not work for this).
* If this is not possible for you, login to ipvslogin, open firefox there and visit the page:

  .. code-block:: bash
    
    ssh -X username@ipvslogin.informatik.uni-stuttgart.de
    # enter initial password
    
    firefox https://www.ipvs.uni-stuttgart.de/de/abteilungen/ifs/rechnerlabor/rechenbetrieb-studenten/passwort
    
* Now you should be able to login with your own password

If you cannot login, please ask in the exercise.

.. _howtogetontothecluster:

How to get onto the cluster
^^^^^^^^^^^^^^^^^^^^^^^^^^^
We provide a compute cluster consisting of 4 connected nodes, each with a recent processor (2x AMD EPYC 9274F CPUs, 4.05GHz, quad-core, 256MB cache, 384GB DDR5 RAM).
However, the cluster is not directly accessible from the network.

* You have to first login to the *login server* `ipvslogin.informatik.uni-stuttgart.de` (or simply `ipvslogin` from within the university network). 
* From the login server, you can connect to the command server of the cluster, `simcl1`. To check how busy the cluster currently is, type `squeue`.
* From there, you can enter an interactive shell on a node of the cluster with the `srun --pty bash` command. The nodes have hostnames `simcl1n1` - `simcl1n4`.

Use `ssh -X` to enable *X-forwarding*, i.e. to be able to have graphic windows displayed on your computer. In summary:

.. code-block:: bash
  
  ssh -X username@ipvslogin.informatik.uni-stuttgart.de
  # enter password
  
  ssh -X simcl1
  # enter password
  
  squeue
  srun --pty bash
  # now you are on a node of the cluster
  
Please don't compile or run programs on `ipvslogin` because it is not so powerful and its purpose is only to enable login from outside the university. Use simcl1 or the cluster nodes instead.

You will learn how to employ multiple nodes of the cluster in exercise 2.

Note, you can avoid having to type the password all the time, by generating a pair of private and public keys on each server, setting proper file permissions and adding the public key to the `~/.ssh/authorized_keys` on the host that would ask for your password. Find out the details yourself, e.g. `here <https://www.thegeekdiary.com/centos-rhel-how-to-setup-passwordless-ssh-login/>`_.

When you are on any simcl1 or simcl1n* node, you should load the following modules, which make the respective software modules available:

.. code-block:: bash
    
  module use /usr/local.nfs/sgs/modulefiles
  module load gcc/10.2
  module load openmpi/3.1.6-gcc-10.2
  module load vtk/9.0.1
  module load cmake/3.18.2
