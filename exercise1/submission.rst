Submission Exercise 1
-------------------------

Write a program that solves the two-dimensional, incompressible Navier-Stokes Equations with Dirichlet boundary conditions on a two-dimensional, rectangular domain with a Finite Differences approach, as discussed in the lecture. The program has to be able to parse a given parameter file and simulate the scenario using the given parameters. Several different parameter files with scenarios similar to the lid-driven cavity will be tested.

Implement the central differences scheme as well as the Donor Cell scheme. Which scheme to use should be determined by the value in the parameter file.
Implement at least the SOR scheme, the Gauss Seidel scheme is voluntary.

Submit your code without the `build` folder through the `submission system <http://opendihu-ci.informatik.uni-stuttgart.de/>`_.
You can create a `submission.zip` archive by executing the following in your workspace directory:

.. code-block:: bash

  zip -r submission.zip src/ CMakeLists.txt 
  
The following sequence of commands should produce resulting ".vti" files in the "out" subfolder of `build`, as implemented by the `provided "OutputWriterParaview" <https://gitlab.com/ipvs_sgs/numsim_exercises/raw/master/exercise1/resources/output_writer.zip>`_ class.
Note that only the ".vti" output files are required, the ".txt" are just for your convenience, you can disable them as they only slow down the program.

.. code-block:: bash
  :linenos:
    
  unzip submission.zip                    # this unzips the uploaded archive, filename may be different
  mkdir -p build && cd build
  cmake -DCMAKE_BUILD_TYPE=Release ..
  make install
  ./numsim lid_driven_cavity.txt          # instead of lid_driven_cavity.txt, other files will be tested and should produce the respective results
  
Your code has to follow these rules:

#. No third-party libraries other than libvtk are allowed.
#. The code must only solve the exercise and not interfere in any way with the submission system, e.g. by deleting files. Hacking is not allowed.
#. The produced results have to be correct.
#. The code has to be object-oriented, properly commented and have a reasonable code quality. It can but does not have to follow the presented sample architecture.
#. The code should be efficient in a reasonable way.

.. #. Bonus points may be given for: Additional solvers like Gauss-Seidel and Conjugate Gradients. A runtime lower than 10% of the runtime of the reference implementation, if the results are correct, high code quality like const-correctness, doxygen-style comments.

\1. and especially 2. are disqualification criteria. 3. is the main criterion for the final score. 4. and 5. can give bonus or malus points if done exceptionally well or failed terribly.


Questions for the interview ("Abnahme")
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Choose the time steps manually. For which time steps :math:`dt` is the algorithm stable?
* What happens when the Reynolds number is changed: Re = 100, 500, 2000, 10000?
* We have claimed in the lecture that our discretization in space is second order accurate (if using central differences), respectively, first order accurate (if using the Donor Cell schemes). The time discretization is first order accurate. How could you verify this using your simulation program?
* List the run-times for different grid resolutions in a table (bring it to the meeting, one per group). Do the results match our theoretical considerations from the lecture? Why / Why not?
