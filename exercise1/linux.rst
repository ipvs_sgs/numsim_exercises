Getting started with Linux
-----------------------------

If Linux is completely new for you, take the time to read a tutorial to get started with the command line. `This tutorial <https://tutorials.ubuntu.com/tutorial/command-line-for-beginners>`_ is a good start.

Terminal
^^^^^^^^^^

As we will often need the command line in different directories, it is benefical to open multiple tabs in the terminal. There is also the possibility to install the program `terminator`, a terminal emulator:

.. code-block:: bash
  
  sudo apt install terminator
  
It allows to split the window into multiple terminals next to each other, as can be seen in :numref:`terminator`. This is done by a right click and then choosing  *Split Horizontally* or *Split Vertically* in the menu.

.. _terminator:
.. figure:: images/terminator.png
  :width: 50%

  Terminator window with three sub terminals und multiple opened tabs

When logged in at any of the servers of the institute (recall :ref:`howtogetontothecluster`), you can use *tmux*. 
*Tmux* is a terminal program that also allows to split the terminal to multiple panels and create multiple tabs. Furthermore, 
it continues to run your commands even when you log out. When logging back in, you can continue your work where you left.

Try the following commands e.g. on *simcl1*.

To start a new tmux session, run 

.. code-block:: bash
  
  tmux
  
A new session with green border opens and you can type commands as usual. To detach from the session, type *Ctrl+B → let go keys → D*. To attach the next time, type

.. code-block:: bash

  tmux at

When attached, other useful key combinations are: 

* `Ctrl+B "` to split horizontally
* `Ctrl+B %` to split vertically
* `Ctrl+B arrowkey` to change between panels
* `Ctrl+B x` to close current pane (confirm with *y*)
* `Ctrl+B c` to create a new tab 
* `Ctrl+B 0` and `Ctrl+B 1` to switch between tabs 0 and 1. Analogous with more tabs.
* To enable scrolling, execute the following once:

  .. code-block:: bash
    
    echo "set -g terminal-overrides 'xterm*:smcup@:rmcup@'" >> ~/.tmux.conf
    echo "set-option -g history-limit 3000000" >> ~/.tmux.conf
  
  Restart tmux. Then, `Ctrl+B PageUp` and `PageDown` scroll back and forth through the previous output. All commands continue to run in the meantime. Press Ctrl+C once to exit this view.

See the `full list of shortcuts <https://gist.github.com/MohamedAlaa/2961058>`_.

Editors and IDE
^^^^^^^^^^^^^^^^^^^
On your computer, you can use the integrated development environment (IDE) of your choice or any editor. A suggestion would be Visual Studio Code, which recently became quite popular or kdevelop on Kubuntu. 
Both tools are not available on the servers. Instead, there exist simple graphical text editors, 

.. code-block:: bash
  
  geany
  
and 

.. code-block:: bash
  
  gedit

It is not possible to launch those graphical editors inside of *tmux*. But you can open two separate terminals, one where you open, e.g., geany and the other one with tmux and your compilations and simulation runs.

While graphical editors may be handy because they can be controlled by keyboard and mouse, copy & paste is easy and scrolling is easily possible, experienced users may prefer "terminal-only" editors.
They can be used whereever terminal access is availabe, e.g., over network, inside virtualized containers, on compute nodes of supercomputers where no graphics system is installed, etc.

A very popular editor that runs in the terminal and thus in tmux, is **vim**. 
After launching `vim <your filename>`, press `i` to switch to *insert mode*. Now you can type in and delete text. If you want to save, press `Esc` to leave insert mode.
Type `:wq` to save and exit or `:q!` to exit without saving.

Copying files to and from the servers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In order to copy files between remote hosts, the *secure copy* command `scp` can be used. It needs two arguments: source and destination of the file to be copied.
For example, if you have a file *file.txt* in your current working directory and want to copy it to `ipvslogin` to a directory `src` in the home folder (or the reverse direction), the commands are the following:

.. code-block:: bash
  
  # copy file to server
  scp file.txt maierbn@ipvslogin:src
  
  # copy the same file back
  scp maierbn@ipvslogin:src/file.txt .

Here, replace *maierbn* by your user name (account). The user name and the '@' sign can be omitted if the user name is the same on both hosts. E.g. if you copy between *ipvslogin* and *simcl1*, you don't need the user name.
Note, how the remote server is indicated by adding the colon ":". The path afterwards is relative to the home directory on that server. The dot "." in the second command specifies the current directory as the target where the file should be copied to.

To copy a whole directory, the recursive *-r* option can be used:

.. code-block:: bash
  
  # copy a whole directory to server
  scp -r numsim maierbn@ipvslogin:src
  
  # copy the same directory back
  scp -r maierbn@ipvslogin:src/numsim .

It is not possible to directly copy anything from your computer to *simcl1*. Instead an intermediate step to *ipvslogin* and from there further to *simcl1* would be needed.
However, the home directories are shared between all hosts. This means that if you copy a file to *ipvslogin:* it will automatically appear also on *simcl1* and the compute nodes *simcl1n1* to *simcl1n16*.
