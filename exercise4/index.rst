.. warning::
   This is an old exercise from another year and only provided as reference on how to get started with preCICE.
   It uses free geomentries and a heat transport solver, which we did not implement in the previous exercises. 

===============
Exercise 4
===============

The goal of this exercise is to implement bi-directional **coupling** using the coupling library preCICE. The solvers will exchange the temperatures :math:`T` and heat flux :math:`q`.

.. note::
   Make sure that the heat transport equation and obstacles have been implemented correctly in the previous exercise. 

.. toctree::
   :maxdepth: 1
   :caption: Contents
   
   precice
   coupling
   solidsolver
   testcases
   hints
   submission
   
