.. _ex4-precice:

=============================
The coupling library preCICE
=============================

In this exercise we want to use the coupling library preCICE. We will use the current version (1.6.1) that has been release in september. 

   "preCICE is an open-source coupling library for partitioned multi-physics simulations, including, but not restricted to fluid-structure interaction and conjugate heat transfer simulations. Coupling adapters exist for OpenFOAM, CalculiX, Code_Aster, FEniCS, deal.II, and many more."
   
   Taken from the `preCICE homepage`_ 


Getting started
---------------

You can learn more about preCICE on the `preCICE homepage`_ and the `GitHub repository`_ of preCICE. Checkout the `preCICE wiki`_ that explains how to get and use preCICE. It also provides a lot of examples of coupled simulations run with preCICE. In order to get a feeling for how preCICE works you should go through the `preCICE tutorial in browser`_. You don't have to install anything on your machine for this tutorial and the test setting is also explained on the wiki in the section `FSI tutorial`_.

Installation
----------------

There are plenty ways of getting preCICE. We have installed preCICE on the cluster so you can use it. The easiest way would be via the precompiled package available on GitHub. Alternatively preCICE can be compiled using ``CMake``. Compilation should be easy when using Ubuntu 18.04 as the required dependencies can be installed from the package manager.

For the exercises you do **not** need to compile preCICE with all features! Besides things you already have (C++ compiler and CMake) you mainly need:

- Eigen
- Boost (version >= 1.65.1)
- libxml2
- (Optional) a MPI compiler if you want to use preCICE in parallel

For detailed information about all the ways to install preCICE and how to do it, please check out the section `1. Get preCICE` in the `preCICE wiki`_.

.. warning::
   
   Please make sure that you build with shared libraries, i.e. configure the preCICE with ``-DBUILD_SHARED_LIBS=ON``!

.. note::

   It could be beneficial to compile preCICE as *debug* build for testing. The output will be more verbose such that it is easier to find problems. For productive runs it makes sense to compile preCICE as *release* build. Both builds will be provided on the cluster.

Preparing your code to use preCICE
-----------------------------------

In order to use preCICE in your code, we have to edit the file ``CMakeLists.txt`` in the ``src/`` directory of your project and set the necessary environment variables. You have to add the following lines to the ``CMakeLists.txt`` file:

.. code-block:: cmake

   # Add preCICE to project
   find_package(precice REQUIRED CONFIG)
   target_link_libraries(${PROJECT_NAME} PRIVATE precice::precice)

If you get an error afterwards, you might have to replace the line

.. code-block:: cmake

   target_link_libraries(${PROJECT_NAME} ${VTK_LIBRARIES})
   
by 

.. code-block:: cmake

   target_link_libraries(${PROJECT_NAME} PRIVATE ${VTK_LIBRARIES})

Depending on the installation method that you use, you also have to set some environment variables. You can either set the ``CMAKE_PREFIX_PATH`` or ``precice_DIR`` variable. We will set ``precice_DIR`` in the preCICE module on the cluster. In order to set the variable in the terminal (for example in bash) you can use the following command

.. code-block:: bash

   export precice_DIR=PathToPrecice/lib/cmake/precice

If it works you can make it permanent by adding the command to your ``.bashrc`` in your home directory (``~/.bashrc``).

If you run in any other problems, please let us know. 

The preCICE wiki explains how to do the `Linking to preCICE` in more detail. 

.. note::
   
   You can also look in the solver dummy we wrote, see :ref:`ex4-coupling-verification`. The code shows how to use preCICE. It does **not** support implicit couplings at the moment.

Using preCICE on the cluster
----------------------------

We provide two modules (one debug build and one release build) on the cluster. The preCICE modules will set the environment variable ``Dprecice_DIR`` such that CMake should be able to find it. 

.. note::
   
   This part will be updated soon to contain information about the preCICE installation on the cluster.

Getting help and providing feedback
-----------------------------------

If you have questions concerning preCICE, please use the proper channels of communication:

- For smaller questions you can use `Gitter`_. The main developers and other users are often there to help.  
- Bigger questions should be asked on `Discourse`_.
- If you find any bugs, have an idea for improvements etc. please open an issue in the `GitHub repository`_. Check that there does not already exist an issue!

We would be very happy if you would provide feedback on the usability and the documentation. You can use the `feedback form`_ that is also linked on the `preCICE homepage`_.

Tasks
-----

- Get familiar with preCICE. 
- Go through the interactive `preCICE tutorial in browser`_.
- Install preCICE 1.6.1 on your local machine, please follow the installation instructions (also on the wiki). Run the tests in order to see that everything works as expected.
- Check out the preCICE `C++ API`_ to get a feeling which function calls exists. You will only need a subset of the commands here. 
- Go through the `adapter example`_ carefully. You will have to write your own adapter(s) for your code. The example covers all function calls that you will need in this exercise. 
- Adapt your CMake files such that your code can be linked against preCICE.

.. _GitHub repository: https://github.com/precice/precice
.. _preCICE wiki: https://github.com/precice/precice/wiki
.. _preCICE homepage: https://www.precice.org/
.. _Linking to preCICE: https://github.com/precice/precice/wiki/Linking-to-preCICE
.. _FSI tutorial: https://github.com/precice/precice/wiki/FSI-tutorial
.. _preCICE tutorial in browser: http://run.precice.org/
.. _Discourse: https://precice.discourse.group/
.. _C++ API: http://xgm.de/precice/docs/develop/classprecice_1_1SolverInterface.html\
.. _adapter example: https://github.com/precice/precice/wiki/Adapter-Example
.. _Gitter: https://gitter.im/precice/Lobby
.. _feedback form: https://precice.typeform.com/to/IeiyKF
