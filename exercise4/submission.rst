===========
Submission
===========

.. warning::
   The exercise will be updated soon. Some information has changed.

**Deadline**: Please see the time table in `Ilias <https://ilias3.uni-stuttgart.de/>`_.

In case you find any errors, have suggestions for improvements or have the feeling that anything is unclear, please get in touch with us. It will be harder to contact us during the Christmas holidays so please make sure to start with the exercise before such that you still have time to ask questions.

Contact: `Alexander Jaust <mailto:alexander.jaust@ipvs.uni-stuttgart.de>`_

Submission system
-----------------

You will use the same submission system as before. It will be extended such that it can run coupled simulations with preCICE. Please make sure that your CMakeFile creates two executables ``numsim_fluid`` and ``numsim_solid`` when ``make install`` is invoked. 

We try to have shorter tests than in the previous exercise. The test cases run by the submission system will be based on the test cases in this exercise. The maximum runtime is set to 180 seconds per test case. The two mandatory test cases are used in the testing system. We have added some additional debugging output of the sample code to the test case section that might be useful for debugging the code, see :ref:`ex4-test-cases`. Please not that the test system will show output any output of the simulations as it would be too much. Only the compilation process will be shown.

The ``compare_solution`` executable has been adapted. There exist two executables. One is for the fluid and one for the solid solver. You can find it here: :download:`Download file <resources/compare_solution.zip>`.

Rules
-----

The same rules as in the precious exercises apply: No external libraries except VTK, MPI, and preCICE are allowed, no hacking, reasonable object-orientation and comments in the code are necessary. (We may check the source code if at the end your average grade is exactly between two possible grades.)

Questions for the interview (“Abnahme”)
---------------------------------------

- Name advantages / disadvantages of the used library approach for coupling over a server-based approach.
- Try to vary the difficulty of the coupling problem by varying the conductivity values in fluid and solid. Which configurations are challenging (make the pure fixed-point iteration diverge)?
- Think of a solution to the non-converging coupling iterations for those challenging problems without using under-relaxation or quasi-Newton. Hint: Play with the coupling conditions and the resulting use of boundary conditions for fluid and solid.
