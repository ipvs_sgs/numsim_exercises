.. _ex4-test-cases:

==========
Test cases
==========

We will provide input and geometry files for all relevant test cases. These test cases will be part of the submission system. 

.. note::
   The test cases here should be a safe baseline for developing your code. Please play around with different coupling strategies and the parameters controlling preCICE. Study how the parameters affect the simulation (number of coupling iterations, solution etc.) and when the coupling gets unstable.

.. note::
   
   17 Dec 2019: The remaining test cases have been updated.   

   13 Dec 2019: The first test case has been updated.
   
   07 Jan 2020: Fixed error in solid solver input files (``readDataName``/``writeDataName`` where swapped) and added additional results for debugging.


.. _ex4-test-forcedconvection:

Forced convection over a heated plate
=====================================

We have a rectangular geometry for the channel. The fluid enters the domain from the due to a pressure gradient of :math:`\delta p = 0.005`. The Reynolds number is :math:`\mathrm{Re}=500` and the Prandtl number is :math:`\mathrm{Pr}=0.01`. Gravity and volumetric expansion coefficient are set to zero. 

The solid is heated from below such that its bottom temperature is fixed at :math:`T_{\mathrm{solid,bottom}}=0`. Its heat diffusivity is set to :math:`\alpha = 0.2` and so is its initial temperature.

.. figure:: images/flowoverheatedplate.png
   :scale: 30%
   :align: center

   Sketch of the domain of the forced convection over a heated plate.

Explicit coupling
-----------------

You can find the detailed setting in the :download:`zip-file <resources/forcedconvection.zip>`. The zip-file also contains some helper scripts (``Allrun.sh`` and ``Allclean``) that will run both solvers (must be in same directory) and remove all simulation results. A precice-configuration for explicit coupling is included 

.. figure:: images/forcedconvection.png
   :scale: 40%
   :align: center

   Solution obtained with the input files below.


Debugging
~~~~~~~~~

In order to help with debugging you can find a few time steps with debugging output here: :download:`Download files <resources/forcedconvection_debug.zip>`.

Implicit coupling
-----------------

The test case in the given configuration works with the explicit coupling scheme. You can also try to run it with an implicit coupling scheme. 

.. _ex4-test-naturalconvection:

Natural convection in a cavity with heat-conducting walls
=========================================================

The second test case is similar to the natural convection test cases from the precious exercise sheet. This time the fluid domain is not heat directly, but it is surrounded by a solid enclosure which is heated on the left and cooled on the right. 

You can find the problem setup below:

.. figure:: images/naturalconvection_setup.png
   :scale: 30%
   :align: center

   The setup of the natural convection test case. The fluid domain is marked blue and the solid domain is marked in white.

The initial temperature of the solid is set to :math:`T_{\mathrm{solid,init}}=303` and the heat diffusivity of the solid is :math:`\alpha_{\mathrm{solid}}=1`. We set the fluid the initial temperature is set to :math:`T_{\mathrm{fluid,init}}=303`. Prandtl number and Reynolds number are set to :math:`\mathrm{Pr} = 0.01` and :math:`\mathrm{Re}=1000`. Additionally, we enable gravity :math:`\vec{g}=(0, -9.81)^T` and the volumetric expansion coefficient :math:`\beta = 0.2`.

Below you can find the sample solution at different times:

.. figure:: images/naturalconvection_tend_0_1.png
   :scale: 30%
   :align: center

   Solution obtained at :math:`t=0.1`.

.. figure:: images/naturalconvection_tend_0_2.png
   :scale: 30%
   :align: center

   Solution obtained at :math:`t=0.2`.

.. figure:: images/naturalconvection_tend_0_3.png
   :scale: 30%
   :align: center

   Solution obtained at :math:`t=0.3`.

One can observe how the flow inside the fluid domain starts twisting the temperature field. The interface separating the solid domain and the fluid domain is indicated by the white line inside the domain. The solid solver marks the inner part of its domain as "inner obstacles" such that no solution is computed here. Therefore, a lot of values of the solution are set to ``nan`` there:

.. figure:: images/naturalconvection_solid.png
   :scale: 30%
   :align: center

   The simulation domain of the solid solver.

Implicit coupling
-----------------

You can find the files used to compute the solution presented above in the :download:`zip-file <resources/naturalconvection.zip>`. Note that the parameter ``endTime`` in the ``input`` files is set to :math:`10`! The time loop of the simulation should be controlled by preCICE such that the simulation actually ends for :math:`t<10`.

Play around with the parameters (coupling scheme, time step size etc.)! See how the solution develops for larger simulation times (``max-time`` in the preCICE-configuration file).

Debugging
~~~~~~~~~

In order to help with debugging you can find a few time steps with debugging output here: :download:`Download files <resources/naturalconvection_debug.zip>`.



Explicit coupling
-----------------

You can use the provided files to set up an explicit coupling scheme. Does it work?

.. _ex4-test-heatexchanger:

2D heat exchanger (optional)
============================

A heat exchanger is a tool that heats up a cold target fluid (here fluid domain 2) by a warm fluid (here fluid domain 1) without mixing both fluids. The two fluid domains, see figure below, are aligned at the four thin gray bars. The four bars are the solid participant. Depending on your implementation, you may need to adjust the geometry files. Experiment with different shapes, parameters and ‘turning on’ gravity, to allow for a better exchange of heat.

.. figure:: images/heatexchanger_fluid1.png
   :scale: 30%
   :align: center

   Domain of fluid 1.

.. figure:: images/heatexchanger_fluid2.png
   :scale: 30%
   :align: center

   Domain of fluid 2.

.. note:: 

   It is **optional** to run the test case. 

   If you want to run this test case and need help with setting up the domain and configuration files, please get in touch with us.


