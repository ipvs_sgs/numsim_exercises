.. _ex3_examples:

========
Examples
========

Due to the modifications of your code, we are now able to compute a whole bunch of new examples. We onphysicalSizeY need to set up a new input and geometry file for each example. 

Below we you find several test cases that your solver should handle. The parameters given should work with your solver. Play around with the test cases. What happens if you change some of the parameters or the boundary conditions? 
   
.. note::
   For all example problems, alpha refers to the donor-cell coefficient and not to the thermal diffusivity.   

.. note::

   Update (28 Nov 19): The test cases have been updated. They should work now with the given settings.

   Update (27 Nov 19): The test cases have been updated.
   
   Update (26 Nov 19): We have updated the geometry and the geometry generator for the backward facing step. The initial version had wrong boundary conditions. 

Kármán Vortex Street
--------------------

The flow in a channel hits a tilted plate. At the left boundary, the fluid inflow has a **constant** velocity profile (:math:`(u,v)=(1,0)`), while at the upper and lower boundaries  no-slip conditions are imposed. The plate occupies one fifth of the channel width and is three cells thick. The distance from the left boundary is assumed to be the same as the distance to the lower and upper boundaries. Make sure that there are no forbidden obstacle cells. Use the following values for the parameters:

.. figure:: images/karmanvortex.png

   Sketch of domain for Kármán vortex street test case. Noslip walls are use at the top and bottom of the channel (not indicated in the current figure).

================================ =========== =========== ========= 
 Parameter                         Value     Parameter    Value  
================================ =========== =========== ========= 
 (nCellsX, nCellsY)               (100, 20)   gamma         0.5
 (gX, gY)                            (0, 0)   tau           0.5
 (physicalSizeX, physicalSizeY)     (10, 2)   alpha         0.9
 Re                                   10000   omega         1.7
 Pr                                       1   epsilon      0.001
 beta                                     0   maxIter       500
 timeEnd                                500            
 uInit                                     1   pInit           0
 vInit                                     0   tInit           0     
================================ =========== =========== ========= 

You can download some example geometries and an input file:
:download:`Download files <resources/karmanvortex/karmanvortex.zip>`. This also includes a coarser mesh in a shorter channel with simplified geometry. Don't expect the solver to act the same as for the finer mesh and the full domain.

You can also try to run the same test case with a **parabolic** velocity profile at the inflow boundary: :download:`Download files <resources/karmanvortex/karmanvortex_parabola.zip>`

Use the tools provided by ParaView to characterize the flow around the obstacle. More information about the Kármán vortex street can be found (`on Wikipedia`__).


The solution around the obstacle should look similar to the following figure:

.. figure:: images/results//karmanvortex.png
   :scale: 20 %
   :align: center

.. note::
   We discuss this problem without heat transfer. Therefore, set the temperature to 0 at every boundary and set the Prandtl number to 1 and the volumetric expansion coefficient to 0.
   
   The pressure solver might not converge in each time step (especialphysicalSizeY for the first time steps). The velocity should still look reasonable. 
   
   If the simulation takes too long, choose a lower value for ``timeEnd``. You should still be able to observe the time dependent behavior of the flow.

Flow over a Backward Facing Step
--------------------------------

The fluid flows through a channel widening on one side. On the left hand inflow we specify pressure boundary conditions (:math:`p_{\mathrm{in}} = 10`) and we do the same on the outlet (:math:`p_{\mathrm{out}} = 9.96`) such that the flow is driven by the pressure difference. No-slip conditions are imposed at the upper and lower walls. The obstacle domain is represented by a square filling up half of the channel height. Use the following values for the problem parameters:

.. figure:: images/backwardfacingstep.png

   Sketch of the domain of the backward facing step.

================================ =========== ============= ========= 
 Parameter                          Value     Parameter      Value  
================================ =========== ============= ========= 
 (nCellsX, nCellsY)                (100, 20)   gamma           0.5
 (gX, gY)                             (0, 0)   tau             0.5
 (physicalSizeX, physicalSizeY)      (10, 2)   alpha           0.9
 Re                                      100   omega           1.7
 Pr                                        1   epsilon       0.001
 beta                                      0   maxIter        5000
 timeEnd                                 500   maximumDt       0.5   
 uInit                                     0   pInit             0
 vInit                                     0   tInit             0   
================================ =========== ============= ========= 

You can find an example input file, a Python script to generate the geometry and and a sample geometry here: :download:`Download files <resources/backwardfacingstep/backwardfacingstep.zip>`

The solution close to the step should look similar to the following figure:

.. figure:: images/results/backwardfacingstep.png
   :scale: 20 %
   :align: center

.. note::
   We discuss this problem without heat transfer. Therefore, set the temperature to 0 at every boundary and set the Prandtl number to 1 and the volumetric expansion coefficient to 0.

Natural convection
------------------

In the previous example, we always had forced convection, we prescribed a fixed velocity at some boundary. Now, with the added energy transport, we can also simulate flows solephysicalSizeY driven by gravity. Use the setup as shown in the picture, all boundaries have no-slip conditions. Use both of the provided parameter values

.. figure:: images/naturalconvection.png

   Sketch of the setup for the natural convection test case.

**Setting 1**

================================ =========== =========== ========= 
 Parameter                          Value     Parameter    Value  
================================ =========== =========== ========= 
 (nCellsX, nCellsY)                (50, 50)   gamma         0.5
 (gX, gY)                         (0, -1.1)   tau           0.5
 (physicalSizeX, physicalSizeY)      (1, 1)   alpha         0.5
 Re                                    1000   omega         1.7
 Pr                                       7   epsilon       1e-5
 beta                               0.00021   maxIter       100            
 T_h                                    1.0   T_c             0 
 endTime                               1000   maximumDt    10.0
 uInit                                    0   pInit           0
 vInit                                    0   tInit           0
================================ =========== =========== ========= 

The solution should look similar to the following figure:

.. figure:: images/results/naturalconvection1.png
   :scale: 20 %
   :align: center

**Setting 2**

================================ =========== =========== ========= 
 Parameter                          Value     Parameter    Value  
================================ =========== =========== ========= 
 (nCellsX, nCellsY)                (50, 50)   gamma         0.5
 (gX, gY)                         (0, -1.1)   tau           0.5
 (physicalSizeX, physicalSizeY)      (1, 1)   alpha         0.5
 Re                                   20000   omega         1.7
 Pr                                       7   epsilon       1e-5
 beta                               0.00021   maxIter       100
 T_h                                    1.0   T_c             0       
 endTime                              10000   maximumDt      2.0
 uInit                                    0   pInit           0
 vInit                                    0   tInit           0         
================================ =========== =========== ========= 

The solution should look similar to the following figure:

.. figure:: images/results/naturalconvection2.png
   :scale: 20 %
   :align: center

A Python script to generate the geometry and sample input files can be found here:
:download:`Download files <resources/naturalconvection/naturalconvection.zip>`

Rayleigh-Bénard Convection
--------------------------

Please read through the full article `on Wikipedia`__.

Please read through the complete article. Use the provided parameter values as a baseline, but experiment with different geometric settings and resolutions. All boundaries have again no-slip conditions. 

Setting 1
~~~~~~~~~


.. warning::
   This test case (onphysicalSizeY setting 1) might run very long.

.. figure:: images/rayleighbenardconvection.png

   Sketch of the setup for the Rayleigh-Bénard convection test case.

================================ ============= =========== ========= 
 Parameter                          Value       Parameter    Value  
================================ ============= =========== ========= 
 (nCellsX, nCellsY)                   (85, 18)   gamma         0.5
 (gX, gY)                         (0, -0.3924)   tau           0.5
 (physicalSizeX, physicalSizeY)       (8.5, 1)   alpha         0.5
 Re                                      33.73   omega         1.7
 Pr                                      12500   epsilon       1e-5
 beta                                  0.00179   maxIter       100
 T_h                                    294.78   T_c         291.2    
 endTime                                 45000   maximumDt   100.0
 uInit                                       0   pInit           0
 vInit                                       0   tInit         293          
================================ ============= =========== ========= 
    
A Python script to generate the geometry, a geometry file and sample input files can be found here: :download:`Download files <resources/rayleighbenardconvection/setting1/rayleighbenardconvection.zip>`
    
.. _KarmanVortexStreetWikipedia: https://en.wikipedia.org/wiki/K%C3%A1rm%C3%A1n_vortex_street

__ KarmanVortexStreetWikipedia_

.. _RayleighBenardWikipedia: https://en.wikipedia.org/wiki/Rayleigh%E2%80%93B%C3%A9nard_convection

__ RayleighBenardWikipedia_

Setting 2
~~~~~~~~~

We use a square domain just instead of a rectangular one. The remainder of the setup stays the same. The top is cooled and the bottom is heated. The setting is the same as in [OCBL2008]_ with Rayleigh number :math:`Ra=10^4`. Use the following parameters:

================================ ==================== =========== ========= 
 Parameter                          Value              Parameter    Value  
================================ ==================== =========== ========= 
 (nCellsX, nCellsY)                   (20, 20)         gamma         0.5
 (gX, gY)                           (0, -9.81)         tau           0.5
 (physicalSizeX, physicalSizeY)         (1, 1)         alpha         0.5
 Re                                        100         omega         1.7
 Pr                                          1         epsilon       1e-5
 beta                              0.1019367991845056  maxIter       1e3
 T_h                                       2.0         T_c           1.0    
 endTime                                   100         maximumDt     1.0
 uInit                                       0         pInit           0
 vInit                                       0         tInit         1.0          
================================ ==================== =========== ========= 

The solution should look similar to the following figure:

.. figure:: images/results/rbconv2.png
   :scale: 20 %
   :align: center

A Python script to generate the geometry, a geometry file and sample input files can be found here: :download:`Download files <resources/rayleighbenardconvection/setting2/rayleighbenardconvection_setting2.zip>`

.. [OCBL2008] Nasreddine Ouertatani, Nader Ben Cheikh, Brahim Ben Beya, Taieb Lili: Numerical simulation of two-dimensional Rayleigh–Bénard convection in an enclosure, Comptes Rendus Mécanique, Volume 336, Issue 5, 2008, Pages 464-470, https://doi.org/10.1016/j.crme.2008.02.004.

Setting 3
~~~~~~~~~

Almost the same as setting 2, but we use a rectangular domain. 

================================ ==================== =========== ========= 
 Parameter                          Value              Parameter    Value  
================================ ==================== =========== ========= 
 (nCellsX, nCellsY)                   (80, 20)          gamma         0.5
 (gX, gY)                           (0, -9.81)          tau           0.5
 (physicalSizeX, physicalSizeY)         (8, 1)          alpha         0.5
 Re                                        100          omega         1.7
 Pr                                          1          epsilon       1e-5
 beta                              0.1019367991845056   maxIter       1e3
 T_h                                       2.0          T_c           1.0    
 endTime                                   100          maximumDt     1.0
 uInit                                       0          pInit           0
 vInit                                       0          tInit         1.0          
================================ ==================== =========== ========= 

The solution should look similar to the following figure (right hand part of the solution is missing on the figure):

.. figure:: images/results/rbconv3.png
   :scale: 20 %
   :align: center

A Python script to generate the geometry, a geometry file and sample input files can be found here: :download:`Download files <resources/rayleighbenardconvection/setting3/rayleighbenardconvection_setting3.zip>`

Fluid Trap
----------

To approach more realistic configurations such as those found in lakes, in cooling systems of buildings, or in solar panels, we must also consider internal obstacle structures. Consider the domain shown in the picture. All boundaries have no-slip conditions. Use the given parameters for your simulation. What happens when you switch the sides of the
heated and cold wall?

.. warning:: 
   This test case seems to diverge after some time. Do you observe the same behavior?
   The test case is still under investigation

.. figure:: images/fluidtrap.png
  
   Sketch of the setup for the fluid trap test case.


================================ =========== =========== ========= 
 Parameter              Value     Parameter    Value  
================================ =========== =========== ========= 
 (nCellsX, nCellsY)                (100, 50)   gamma         0.5
 (gX, gY)                         (0, -9.81)   tau           0.5
 (physicalSizeX, physicalSizeY)       (2, 1)   alpha         0.5
 Re                                    10000   omega         1.7
 Pr                                        7   epsilon       1e-5
 beta                                0.00063   maxIter      1000
 T_h                                     0.5   T_c          -0.5
 endTime                                2000   maximumDt    10.0
 uInit                                     0   pInit           0
 vInit                                     0   tInit           0              
================================ =========== =========== ========= 

A Python script to generate the geometry and sample input files can be found here:
:download:`Download files <resources/fluidtrap/fluidtrap.zip>`
