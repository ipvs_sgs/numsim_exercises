//#include <vtkXMLUnstructuredGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkImageData.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkHexahedron.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <cmath>

//#include <experimental/filesystem>
//using namespace std::experimental::filesystem;

#include "boost/filesystem.hpp"
#include <iostream>
using namespace boost::filesystem;

// the data for one timestep
struct MeshData
{
  std::vector<double> u;
  std::vector<double> v;
  std::vector<double> temperature;
};

// the data of one directory, i.e. for multiple timesteps
struct SimulationData
{
  std::vector<double> times;      //< time points of the parsed values
  std::vector<MeshData> values;   //< the values for a single time step, each, corresponding to times
};

void parseData(std::string filename, MeshData &meshData, double &t, const bool ignoreObstacle)
{
  // Create a reader
  vtkSmartPointer<vtkXMLImageDataReader> reader = vtkSmartPointer<vtkXMLImageDataReader>::New();
 
  reader->SetFileName(filename.c_str());
  reader->Update();
  

  if (reader->GetNumberOfPointArrays() == 0)
  {
    std::cout << "Error: File \"" << filename << "\" contains no data." << std::endl;
  }

  vtkSmartPointer<vtkDataSet> dataSet = reader->GetOutputAsDataSet(0);
  vtkSmartPointer<vtkFieldData> fieldData = dataSet->GetFieldData();
  
  /*
  std::cout << "file \"" << filename << "\" contains " << reader->GetNumberOfPointArrays() << " point arrays, "
    << reader->GetNumberOfCellArrays() << "," << reader->GetNumberOfColumnArrays() << std::endl;
  std::cout << " field has " << fieldData->GetNumberOfArrays() << " arrays.";
  */
  
  // get stored time
  if (fieldData->GetNumberOfArrays() > 0)
  {
    vtkSmartPointer<vtkDataArray> array = fieldData->GetArray(0);
    double *value = array->GetTuple(0);
    t = *value;
  }
  else 
  {
    std::cout << "Warning: File \"" << filename << "\" has no time." << std::endl;
  }
  
  // get stored velocity values
  vtkSmartPointer<vtkPointData> pointData = dataSet->GetPointData();
  bool hasTemperature = false;
  for (int arrayNo = 0; arrayNo < pointData->GetNumberOfArrays(); arrayNo++)
  {
    vtkSmartPointer<vtkDoubleArray> array = vtkDoubleArray::SafeDownCast(pointData->GetAbstractArray(arrayNo));
    int nEntries = array->GetNumberOfTuples();
    //int nComponents = array->GetNumberOfComponents();
    //std::cout << "  " << array->GetName() << ", nEntries: " << nEntries << ", nComponents: " << nComponents << std::endl;
    
    if (std::string(array->GetName()) == "velocity")
    {

      meshData.u.resize(nEntries);
      meshData.v.resize(nEntries);

      // loop over values
      for (int entryNo = 0; entryNo < nEntries; entryNo++)
      {
        std::array<double,3> values;
        array->GetTuple(entryNo, values.data());
        meshData.u[entryNo] = values[0];
        meshData.v[entryNo] = values[1];

        if ( std::isnan( meshData.u[entryNo] ) ) {
          meshData.u[entryNo] = 0.;
        }

        if ( std::isnan( meshData.v[entryNo] ) ) {
          meshData.v[entryNo] = 0.;
        }
      }
      // Set values to zero that could have different values
      // depending on how the values are set for obstacle cells
      // Only works for Karman vortex test case and only for given mesh
      if (ignoreObstacle)
      {
        //Hard coded for now
        meshData.u[918] = 0.;
        meshData.u[919] = 0.;

        meshData.u[1019] = 0.;
        meshData.u[1020] = 0.;
        meshData.u[1021] = 0.;

        meshData.u[1121] = 0.;
        meshData.u[1122] = 0.;

        meshData.v[918] = 0.;
        meshData.v[919] = 0.;

        meshData.v[1019] = 0.;
        meshData.v[1020] = 0.;
        meshData.v[1021] = 0.;

        meshData.v[1121] = 0.;
        meshData.v[1122] = 0.;
      }
    }


    meshData.temperature.resize(nEntries, 0.);
    if (std::string(array->GetName()) == "T")
    {
      hasTemperature = true;
      // loop over values
      for (int entryNo = 0; entryNo < nEntries; entryNo++)
      {
        std::array<double,1> values;
        array->GetTuple(entryNo, values.data());
        meshData.temperature[entryNo] = values[0];


        if ( std::isnan( meshData.temperature[entryNo] ) ) {
          meshData.temperature[entryNo] = 0.;
        }
      }
    }
  }
  if ( hasTemperature == false )
  {
    std::cout << "File does not have temperature field!" << std::endl;
  }
}

void parseDirectory(std::string directory, SimulationData &simulationData, const bool ignoreObstacle )
{
  // get files in directory
  std::vector<std::string> filenames;
  
  directory_iterator end_iter; // default construction yields past-the-end
  for (directory_iterator iter(directory); iter != end_iter; ++iter)
  {
    if (is_directory(iter->status()))
      continue;
    
    if (is_regular_file(iter->status()))
    {
      std::string filename(iter->path().string());
      
      if (filename.find(".vti") != std::string::npos)
      {
        filenames.push_back(filename);
      }
    }
  }
  
  // sort files by filename
  std::sort(filenames.begin(), filenames.end());
      
  //std::cout << "Parse data in \"" << directory << "\"." << std::endl;
  
  // loop over filenames and parse files
  for (std::vector<std::string>::iterator iter = filenames.begin(); iter != filenames.end(); iter++)
  {
    std::string filename = *iter;
    //std::cout << "  File " << filename << std::endl;
    double t;
    simulationData.values.emplace_back();
    parseData(filename, simulationData.values.back(), t, ignoreObstacle);
    simulationData.times.push_back(t);
  }
}

double compareData(SimulationData &testData, SimulationData &referenceData )
{
  const bool output = false;


  // loop over entries in test data
  double differenceNormData = 0;
  double differenceNormTData = 0;
  int nFiles = testData.times.size();

  struct {
    int uvPointIndex = 0;
    int uvFileIndex = 0;

    int tPointIndex = 0;
    int tFileIndex = 0;

//    double uDiff = 0.;
//    double vDiff = 0.;
    double uvDiff = 0.;
    double tDiff = 0.;

  } differenceIndicator;

  for (int i = 0; i < nFiles; i++)
  {
    double t = testData.times[i];
    MeshData &testMeshData = testData.values[i];
    
    if (output)
      std::cout << "test t = " << t << std::endl;
    
    // get corresponding data sets of reference Data
    int firstReferenceIndex = 0;
    int secondReferenceIndex = 0;
    for (int j = 0; j < (int)referenceData.times.size(); j++)
    {
      if (referenceData.times[j] > t)
      {
        secondReferenceIndex = j;
        break;
      }
      firstReferenceIndex = j;
    }
    
    double alpha = 0.0;
    
    if (secondReferenceIndex == 0)
    {
      secondReferenceIndex = firstReferenceIndex;
    }
    else
    {
      alpha = (t - referenceData.times[firstReferenceIndex]) / (referenceData.times[secondReferenceIndex] - referenceData.times[firstReferenceIndex]);
    }
    
    if (output)
    {
      std::cout << "   corresponding reference datasets: " << std::endl
        << "     t=" << referenceData.times[firstReferenceIndex] << " at index " << firstReferenceIndex << std::endl
        << "     t=" << referenceData.times[secondReferenceIndex] << " at index " << secondReferenceIndex << ", alpha: " << alpha << std::endl;
    }
    
    // loop over values
    double differenceNormDataset = 0;
    double differenceNormTDataset = 0;
    int nPoints = testMeshData.u.size();
    for (int j = 0; j < nPoints; j++)
    {
      double testU = testMeshData.u[j];
      double testV = testMeshData.v[j];
      double testT = testMeshData.temperature[j];
      
      double referenceU = (1.-alpha) * referenceData.values[firstReferenceIndex].u[j] + alpha * referenceData.values[secondReferenceIndex].u[j];
      double referenceV = (1.-alpha) * referenceData.values[firstReferenceIndex].v[j] + alpha * referenceData.values[secondReferenceIndex].v[j];

      double referenceT = (1.-alpha) * referenceData.values[firstReferenceIndex].temperature[j] + alpha * referenceData.values[secondReferenceIndex].temperature[j];
      
      double differenceNorm = sqrt( (referenceU - testU)*(referenceU - testU) + (referenceV - testV)*(referenceV - testV));
      const double temperatureNorm = sqrt( (referenceT - testT)*(referenceT - testT) );
      

      if ( differenceIndicator.uvDiff < std::fabs(referenceU - testU) + std::fabs(referenceV - testV) )
      {
        differenceIndicator.uvFileIndex = i;
        differenceIndicator.uvPointIndex = j;
        differenceIndicator.uvDiff = std::fabs(referenceU - testU) + std::fabs(referenceV - testV) ;
//        differenceIndicator.tDiff = std::max(differenceIndicator.tDiff, std::fabs(referenceT - testT) );
      }

      if ( differenceIndicator.tDiff < std::fabs(referenceT - testT) )
      {
        differenceIndicator.tFileIndex = i;
        differenceIndicator.tPointIndex = j;
        differenceIndicator.tDiff = std::fabs(referenceT - testT);
      }

      //std::cout << "      j=" << j << "/" << nPoints << ", error: " << differenceNorm << std::endl;
      differenceNormDataset += differenceNorm;
      differenceNormTDataset += temperatureNorm;
    }
    differenceNormDataset /= nPoints;
    differenceNormData += differenceNormDataset;

    differenceNormTDataset /= nPoints;
    differenceNormTData += differenceNormTDataset;
  }
  differenceNormData /= nFiles;
  differenceNormTData /= nFiles;
  std::cout << nFiles << " files with " << testData.values[0].u.size() << " entries each." << std::endl 
    << "average 2-norm error per velocity vector: " << differenceNormData << std::endl;

  std::cout << "average 2-norm error per temperature: " << differenceNormTData << std::endl;

  std::cout << "Biggest difference uv: " << "File " << differenceIndicator.uvFileIndex
            << " at point " << differenceIndicator.uvPointIndex
            << ", |u-u_ref|+|v-v_ref|: " << differenceIndicator.uvDiff << std::endl;

  std::cout << "Biggest difference T: " << "File " << differenceIndicator.tFileIndex
            << " at point " << differenceIndicator.tPointIndex
            << ", |T-T_ref|: " << differenceIndicator.tDiff << std::endl;
  
  if (fabs(differenceNormData) > 1e-4)
  {
    std::cout << "Velocity error is higher than tolerance of 1e-4!" << std::endl;
  }

  if (fabs(differenceNormTData) > 1e-4)
  {
    std::cout << "Temperature error is higher than tolerance of 1e-4!" << std::endl;
  }

  return differenceNormData;
}

int main(int argc, char *argv[])
{
  // parse command line arguments
  if (argc != 4)
  {
    std::cout << "usage: " << argv[0] << " <testcasename> <directory to test> <directory with reference data>" << std::endl;
    exit(-1);
  }
  
  const std::string testCaseName = argv[1];
  const std::string testCaseIdentifier = testCaseName.substr(0, testCaseName.find("_") );
  //std::cout << "Test case: " << testCaseName << std::endl;
  //std::cout << "Test case identifier: " << testCaseIdentifier << std::endl;
  const bool ignoreObstacle = ( testCaseIdentifier == "karmanvortex" ) ? true : false;
  //std::cout << "Going to ignore obstace " << ((ignoreObstacle) ? "YES" : "NO") << std::endl;

  const std::string directoryTest = argv[2];
  const std::string directoryReference = argv[3];

  SimulationData testData;
  SimulationData referenceData;
  
  // parse data in two directories
  parseDirectory(directoryTest, testData, ignoreObstacle);
  parseDirectory(directoryReference, referenceData, ignoreObstacle);
  
  if (testData.times.empty() || referenceData.times.empty())
    return -1;

  double endTimeTest = testData.times.back();
  double endTimeReference = referenceData.times.back();

  if (fabs(endTimeTest - endTimeReference) / endTimeReference > 0.1)
  {
    std::cout << "End time does not match! Test: " << endTimeTest << ", Reference: " << endTimeReference << std::endl;
  }
  
  // compare data
  compareData(testData, referenceData);
  
  return 0;
}
       
