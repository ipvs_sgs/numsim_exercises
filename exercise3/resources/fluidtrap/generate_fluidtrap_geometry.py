#!/usr/bin/env python3 

temperatureLeft = 0.5
temperatureRight = -0.5

nx = 100
ny = 50

lx = 2.
ly = 1.

f = open( "heattrap_{}x{}.geom".format(nx, ny), "w" )

f.write( "physicalSizeX = {}\n".format( lx ) )
f.write( "physicalSizeY = {}\n".format( ly ) )

f.write( "nCellsX = {}\n".format( nx ) )
f.write( "nCellsY = {}\n".format( ny ) )


f.write( "\nMesh =\n" )

#Left
f.write( "NSW;TD:{},".format( temperatureLeft ) )
# Top
for i in range(1,nx+1):
  f.write( "NSW;TN," )
# Right
f.write( "NSW;TD:{}\n".format( temperatureRight ) )

for j in range(1,ny+1):
  #Left
  f.write( "NSW;TD:{},".format( temperatureLeft ) )
  #Fluid domain
  for i in range(1,nx+1):
    if ( i > 65 and i < 68 and j < 40 ):
      f.write( "S," )
    elif ( i > 32 and i < 35 and j > 10 ):
      f.write( "S," )
    else:
      f.write( "F," )
  #Right
  f.write( "NSW;TD:{}\n".format( temperatureRight ) )
  
#Left
f.write( "NSW;TD:{},".format( temperatureLeft ) )
#Bottom
for i in range(1,nx+1):
  f.write( "NSW;TN," )
#Right
f.write( "NSW;TD:{}\n".format( temperatureRight ) )


f.close()

