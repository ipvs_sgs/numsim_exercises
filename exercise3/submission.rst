========================
Submission of exercise 3
========================

**Deadline**: Please see the time table in `Ilias <https://ilias3.uni-stuttgart.de/>`_.

In case you find any errors, have suggestions for improvements or have the feeling that anything is unclear, please get in touch with us. 

Contact: `Alexander Jaust <mailto:alexander.jaust@ipvs.uni-stuttgart.de>`_

Tasks
-----

You should extend your code in different directions. Firstly, you should extend your program to include heat transport. This will allow to model problems like heat-induced flows. Moreover, you should implement the general boundary conditions (inflow, outflow...) that were introduced described in the lecture notes. In order to model arbitrary domains you have to extend your solver to use a marker-cell based approach. The geometry and boundary conditions will be specified in a new input file that you need to parse. 

The extensions will increase the ability of your solver to model a wide range of different applications without the need to recompile it the whole time. During the extension of your solver you should also introduce the functionality to set initial conditions for the unknowns :math:`u, v, p, T`.  

Checkout out the :ref:`ex3_examples` and try to run them. Read the problem description carefully for additional tasks. Play around with solver parameter and see how the solution changes.

Check the sections :ref:`ex3-heattransport`, :ref:`ex3-boundary-and-geometries` and :ref:`ex3_examples` for additional information.

The result has to be written to ``.vti`` output files using the provided output writer. There should be one output file every ``outputFileEveryDt`` simulation seconds. For example, if ``tendTime=10`` and ``outputFileEveryDt=2``, the program should output 5 or 6 output files (the output for t=0 is optional). The “.txt” file output is not needed and should be disabled as it only slows down the program. The is the same requirement as in the previous exercise. Extend your program to have a parameter ``outputFileEveryDt``. This is a slight variation of what you hat do to in the previous exercise. By default the value of ``outputFileEveryDt`` should be zero. Make sure that the **solution at the final time** :math:`t=t_{\mathrm{end}}` is written once! This might not have been the case in the previous exercise.

In order to clarify the output procedure, have a look at the following pseudocode. It explains how the sample solution decides whether it has to output a file. We added a certain time window of ``1e-4`` where solutions should be written. This should ensure that we output data very close to ``outputFileEveryDt``. The time step size :math:`dt` is **not** changed to match the desired time exactly. 

.. code-block:: c++

    void doSimulation()
    {
      double currentTime = 0.0;
      double lastOutputTime = 0.;
      
      // Initialize problem
      ...
      
      //Time loop
      while ( t < tEnd )
      {
        //Compute time step
        ...
        

        // Advance simulation time      
        currentTime += dt_;
        
        // output data using VTK
        if (currentTime - lastOutputTime > outputFileEveryDt-1e-4)
        {
          outputWriterParaview_->writeFile(currentTime);
          lastOutputTime = currentTime;
        }
      } // End of time loop
      

      // output data using VTK if we did not do this in the last time step
      if ( std::fabs( currentTime - lastOutputTime ) > 1e-4 )
      {
        outputWriterParaview_->writeFile(currentTime);
        lastOutputTime = currentTime;
      }
    }

.. _ex3_submission-system:

Submission system
-----------------

We have extended the submission system so you can choose whether your programm should be executed in parallel. The quantities to be compared are the temperature and the velocity. The ``compare_output`` code has been updated to handle temperature and obstacles and can be found here: :download:`Download compare_solution <resources/compare_solution_with_temperature/compare_output_temperature.zip>`

The executable has to follow the following naming scheme:

    - If you want your submission to run in **parallel**, you have to check the box ("Run in parallel:") in the submission form and the executable that your ``CMakeLists.txt`` produces must be called ``numsim_parallel``.
    - If you want your submission to run in **serial**, you do **not** check the box for parallel exection ("Run in parallel:") in the submission form and the executable that your ``CMakeLists.txt`` produces produce must be calles ``numsim``.

Summary
---------------

- Extend your program such that it accounts for heat transport.
- Implement a function to define initial values for all unknowns :math:`u, v, p, T` and read the initial values from the input file.
- Extend your code such that it can read a geometry file that describes the geometry and boundary conditions.
- Extend your code such that it can deal with general boundary conditions. 
- Extend your code such that it can deal with general geometries. 
- Adapt you code such that it does not output files in each iteration.
- Use the techniques explained in the lecture.

Rules
-----

The same rules as in exercise 1 and 2 apply: No external libraries except VTK and MPI are allowed, no hacking, reasonable object-orientation and comments in the code are necessary. (We may check the source code if at the end your average grade is exactly between two possible grades.)

Questions for the interview (“Abnahme”)
---------------------------------------

- Harm the 2-cells-criterion on purpose. What do you observe and why?
- What’s the discretization order for the geometry approximation at curved domain boundaries in our approach? Give a short reason. 
- Assume that we are allowed to use different grid resolutions at the boundary and in the inner part of the domain and that we use a second order discretization (central differences instead of Donor cell for convection) in the inner domain. By which factor would we have to decrease the mesh width :math:`dx`, :math:`dy` each time we half :math:`dx` and :math:`dy` in the inner domain to reduce the error in both parts by the same factor?

