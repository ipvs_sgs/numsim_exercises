.. _ex3-boundary-and-geometries:

==========================================
General Boundary Conditions and Geometries
==========================================

**Task**: Extend your solver to include initial conditions, general geometries and general boundary condition. 

So far, a rectangular domain :math:`\Omega` was necessary for the simulations. Now your code will be extended such that arbitrary two-dimensional domains can be used. Extend your implementation as explained in the lecture notes using the Marker-and-Cell approach. 

.. note::
   - Keep in mind that we have to respect the 2-cell criterion.
   
.. note::
   04 Dec 19: Added information about handling of obstacle cells

Initial conditions
------------------

Before we start to work on the general geometries we want to extend our program such that we can set intitial conditions for all unknowns. Extend your input file to have the paramters:

- ``uInit``: Initial value for velocity :math:`u` (velocity in x-direction).
- ``vInit``: Initial value for velocity :math:`v` (velocity in y-direction).
- ``pInit``: Initial value for pressure :math:`p`.
- ``tInit``: Initial value for temperature :math:`T`.

and implement a function ``applyInitialConditions()`` that is called before the time loop starts and sets the unknowns to the initial values.

.. note::
   The velocities :math:`(u, v)` inside obstacles can be set to ``NaN``. The same holds for the temperature :math:`T` and pressure :math:`p` inside obstacles as well. Take this into account when applying the initial conditions! This will make obstacles appear yellow (depending on the size) in paraview. For setting a variable to ``NaN`` you can use the function ``std::nan`` from the `standard library <https://en.cppreference.com/w/cpp/numeric/math/isnan>`_.
   
General Geometries
------------------

An example of the marked domain is given in the following figure where blue grid cells (F) mark the fluid, gray grid cells (S) mark solid cells and red grid cells (B) mark boundary cells. We specify the boundary conditions a bit more detailed in the next section.


.. figure:: images/channel_with_obstacle_example.png
   :scale: 70 %
   :align: center
   
   Example of the flow domain of size :math:`13\times8` with a solid obstacle of size :math:`3\times3`.
    
.. note::
   - All obstacles in the test cases will have a width of at least 2 cells.
   - If there are cavities in an obstacle, they have to be at least 2 cells wide.
   - Due to the way ParaView is interpolating data and due to the way we realize obstacles you might observe artifacts when visualizing the solution. Obstacles might appear smaller that they are and especially for small obstacles. You can use your text output to verify that your solver is working correctly.
   - The number of fluid cells might be smaller than :math:`nCellsX \times nCellsY`. You have to account for that when computing the residual norm as we divide there by the number of fluid cells. 

Handling of obstacle/solid cells
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You have to careful to implement the handling of obstacles correctly. Think about the two cases from the lecure:

.. figure:: images/north_and_northeastwall_obstacle.png
   :scale: 40 %
   
   Obstacle with fluid on the top (north of the obstacle) and obstacle with fluid on the top and on the right (northeast of the obstacle).


The temperature takes the same kind of boundary condition as the pressure as we want to have insulated no-slip wall boundary conditions on all obstaces.


In order to be on the same page, we with the implementation we should use the same boundary conditions on obstacle cells. Let us assume that we have a 3x3 obstacle cell: 

.. figure:: images/3x3_obstacle.png
   :scale: 25 %
   :align: center
   
   3x3 obstacle
   
For this obstacle the boundary values on the obstacle have to be set on a meaningful way such that all velocities, preliminary velocities, the pressure and temperature are set properly. In the sample solution we set the following values:

.. figure:: images/obstacle_value_situation.png
   :scale: 25 %
   :align: center
      
   Boundary values on obstacles that are set in the sample solution. Note that the index :math:`i,j` refers to the current obstacle cell.

The sample solution only "knows" about these 9 cases (8 cells touching fluid and inner cells only touching solids). We don't check for any other special cases. This can lead to situations where velocities are set inside the obstacle that are never used. An example is the Karman vortex test case where the sample solution sets the inner velocities :math:`u` to zero (but not :math:`v`!). In the Karman vortex test case we try to ignore critical grip points in the comparison script. Check :ref:`ex3_submission-system` for the updated comparison script.
   
This leads to the situation that all values needed on the obstacle are set properly as indicated below. Values in the inner part of the obstacle don't need to be set.

.. figure:: images/all_values_obstacle.png
   :scale: 25 %
   :align: center

Following this approach should leave enough values set to ```std::nan``` for the obstacle to be visible. Moreover, there should be also some entries in the right hand side and the preliminary velocities that are ```std::nan```. This way it should be easy to see if you use values inside the obstacle that you should not use.

When you velocity magnitude for the Karman vortex street test case then the obstace should look like this:

.. figure:: images/obstacle_karman_vortex.png
   :scale: 40 %
   :align: center
   
   Obstacle for the Karman vortex street test case. The velocity magnitude is plotted. The results might vary depending on the version of ParaView and your settings.

General Boundary Conditions
---------------------------

As mentioned before the boundary conditions should be marked. We will not only mark that boundary conditions are present, but also what kind of boundary conditions should be applied. Implement all boundary conditions mentioned in the lecture notes.

.. note::
   - Remember that these "general boundary conditions" are basically combinations of Dirichlet and Neumann boundary conditions that you have already implemented before.
   - Remember to adapt the formula for :math:`F^{n+1}_{i,j}` and :math:`G^{n+1}_{i,j}` for vertical and horizontal boundaries respectively in case of **outflow**, **slip** or **pressure** boundary conditions!

Input files for Geometry and Boundary Conditions
------------------------------------------------

In order to deal with arbitrary geometries and boundary conditions we will change the format of our input file and introduce an additional input file that describes the geometry and the boundary conditions. 

Extend your input files to include a parameter called `geometryFile`. This will point to a file that describes the geometry and boundary condition. You may remove the parameters ``physicalSizeX``, ``physicalSizeY``, ``nCellsX``, and ``nCellsY`` from your normal input file. The values for the velocities on the Dirichlet boundary conditions (``dirichletBottomX``, ``dirichletBottomY``...) may be removed as well as everything will be specified in the geometry file. However, you can keep these parameters in your input files for your own tests. The new input files used for automatic testing of your submission will not specify these parameter anymore.

Data format of Geometry File
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use a simple file format that is inspired by comma-separated values (`CSV on Wikipedia`_).

- Lines starting with ``#`` are comments and should be ignored.
- The first relevant line contains two entries for the dimension of the domain in x- and y- direction. This is equivalent to the parameters ``physicalSizeX`` and ``physicalSizeY``.
- The second relevant line contains the number of grid cells in x- and y-direction. This corresponds to ``nCellsX`` and ``nCellsY``
- The definition of the mesh (including boundary conditions) follows immediately after the key work ``Mesh =`` appeared. No additional blank lines are allowed after ``Mesh =``.

   - Each boundary cell encodes a boundary condition for the velocity and pressure and another boundarty condition for the temperature. The two conditions are separated by a ``;``.
   - The boundary condition for velocity and pressure is indicated by a short string. Possible values are:
   
      - ``NSW``: Noslip wall
      - ``SLW``: Slip wall
      - ``IN``: Inflow boundary condition. The string ``IN`` is followed by the values for the velocity in x- and y direction. Each are separated by a colon ``:``. An inflow condition with velocities :math:`(u,v)=(2,3)` would be denoted as ``IN:2:3``.
      - ``OUT``: Outflow boundary condition
      - ``PR``: Pressure boundary condition. The string is followed by a colon ``:`` and the value for the pressure. Setting the pressure to 4 would read ``PR:4`` for example.
      
   - The boundary condition for velocity and pressure is indicated by a short string.
   
       - ``TD``: Dirichlet temperature boundary condition (=fixed temperature at surface)
       - ``TN``: Neumann temperature boundary condition (=fixed heat flux through.). If set to zero heat flux we obtain an insulated/adiabatic wall.
       - Both temperature boundary condition strings are followed by a colon ``:`` and the respective values. In order to fix the temperature to :math:`T=300` one would write ``TD:300`` and for insulated walls one would write ``TN:0`` which implies that the heat flux over the boundary is zero. 

.. note::
   You can reuse parts of the parser that you have written for the input file.

Examples:
~~~~~~~~~

- Pressure boundary condition with :math:`p=10` and temperature being fixed to :math:`T=300`: ``PR:10;TD:300``
- Noslip wall and insulated wall: ``NSW;TN:0``
- Inflow boundary with velocity :math:`(u,v)=(1,3)` with constant temperature :math:`T=273`: ``IN:1:3;TD:273``

The required program and files for general geometries with flow only will be introduced in the
tutorial and provided via ILIAS. The program creates the geometry data for the two flow only
examples specified above. Also, it creates the data for different discretizations and sets the
corresponding boundary conditions. An appropriate text-based format would be considered
that is easy to parse. For example, one can think of using the csv-files, for which rows of the
file represent cells of the discretization. The payload in this case would be cell type together
with the corresponding boundary conditions, coded as integers for example.
To make sure that your code is still capable of computing the driven cavity flow, without the
requirement that you create a geometry file, you need a flag in the geometry file that shows if
a geometry should be used (for example `geometry=free`).

Example file
~~~~~~~~~~~~

Below you find two example geometry files. he first one described the lid-driven cavity case that we used for the first two exercises. The second geometry described the lid-driven cavity case with heated top that is described in :ref:`ex3-heattransport-verification`. In both cases we use a very coarse mesh with :math:`5\times5` cells. 

**Lid-driven cavity**

.. literalinclude:: resources/liddrivencavity/liddrivencavity_5x5.geom

**Lid-driven cavity with heated lid**

.. literalinclude:: resources/liddrivencavity/liddrivencavity_heat_5x5.geom

.. _CSV on Wikipedia: https://en.wikipedia.org/wiki/Comma-separated_values

.. _ex3-geometry-verification:

Verification
~~~~~~~~~~~~

In order to verify that the results are correct when the boundary conditions are tead from the input file you can skip the implementation of obstacles first. Implement the parser and the boundary conditions. Then rerun the solver with meshes specifying the boundary conditions. You can generate such meshes with the following Python scripts:

- Lid-driven cavity flow :download:`Download files <resources/liddrivencavity/generate_liddrivencavity_geom.py>`
- Lid-driven cavity flow with heated lid :download:`Download files <resources/liddrivencavity/heat_with_geometryfile/generate_liddrivencavity_withheat_geometry.py>`

Another good test case is the laminar channel flow as there is an analytical expression for the solution available (see lecture notes). Files for this test case an be downloaded here:
:download:`Download files <resources/laminarchannelflow/laminarchannelflow.zip>`

The solution for the channelflow should look similar to the following figure in case one uses a vertical configuration with obstacles on the walls (taken from the submission system). The flow field should be like this in any configuration:

.. figure:: images/results/channelflow_vertical.png
   :scale: 20 %
   :align: center

The zip-file includes a Python script to generate the geometry, a suitable input file and the analytical solution in a csv-file.

**Additional sample solutions**

Some solutions with debugging output (``txt``-files). The first file (``output_0000.txt``) is written **after** the initial conditions have been set, but **before** the time loop. We set all values of an obstacle to ``std::nan``. The values on the obstacles will be set later when the boundary conditions are evaluated. The other text files are written in the time loop **after** I have computed the preliminary velocities, but **before** the right hand side is computed. This is **not** the same as in previously provided text files! 

So ``output_0001.txt`` will contain :math:`u^{(0)}`, :math:`v^{(0)}`, :math:`p^{(0)}`, :math:`T^{(0)}`, :math:`F^{(0)}`, :math:`G^{(0)}` and :math:`rhs^{(-1)}` with boundary conditions already applied.. ``output_0002.txt`` will contain :math:`u^{(1)}`, :math:`v^{(1)}`, :math:`p^{(1)}`, :math:`T^{(1)}`, :math:`F^{(1)}`, :math:`G^{(1)}` and :math:`rhs^{(0)}` with boundary conditions already applied. The last output file contains the data at the very end of the simulation. The very first and last files will have an odd time (-1 and endTime+1).

Lid-driven cavity with heat as in :ref:`ex3-heattransport`: :download:`Download files <resources/liddrivencavity/liddriven_cavity_heat_re316_ri10_sample_solution_t_5.zip>`. 

Karman vortex street up to :math:`t_{end}=1` as in :ref:`ex3_examples`: :download:`Download files <resources/karmanvortex/karmanvortex_debugging.zip>`. 

Laminar channel flow up to :math:`t_{end}=1` as in :ref:`ex3_examples`: :download:`Download files <resources/laminarchannelflow/laminarchannelflow_debugging.zip>`.

Rayleigh-Benard convection (scenario 2) up to :math:`t_{end}=5` as in :ref:`ex3_examples`: :download:`Download files <resources/rayleighbenardconvection/rbconv_scenario2_debugging_tend5.zip>`.  

Rayleigh-Benard convection (scenario 2) up to :math:`t_{end}=55` as in :ref:`ex3_examples`. Mainly Paraview files and the last two text output files. Otherwise the archive gets too large: :download:`Download files <resources/rayleighbenardconvection/rbconv_scenario2_debugging_tend55.zip>`.  

Rayleigh-Benard convection (scenario 3) up to :math:`t_{end}=5` as in :ref:`ex3_examples`: :download:`Download files <resources/rayleighbenardconvection/rbconv_scenario3_debugging_tend5.zip>`.

.. note::
   This will not test all boundary conditions that you have implemented. Make sure you run some other tests, for example the test cases from the :ref:`ex3_examples`.
