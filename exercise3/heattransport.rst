.. _ex3-heattransport:

==============
Heat transport
==============

**Task**: Extend your solver to include heat transport. 

We want to model heat transport via the heat equation

.. math::
   \frac{\partial T}{\partial t} = \frac{1}{Re} \frac{1}{Pr} \left( \frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} \right) - \frac{\partial uT}{\partial x} -  \frac{\partial vT}{\partial y} + q

where :math:`Pr` refers to the Prandtl number and :math:`q` some heat source or sink.

.. note::
   01 Dec 19: Added additional sample solution for the verification test case and recomputed solution that had already been uploaded.
   27 Nov 19: Added a note regarding the parameter :math:`\gamma` that should be included in the input file as new parameter ``gamma``.

.. _ex3-heattransport-implementation:

Implementation
--------------

Implement the discretization of the heat equation as described in the lecture. You can start with this and verify your implementation without implementing general geometries using the test case described below. Possible boundary conditions would be prescribed temperatures (Dirichlet boundary condition) or prescribed heat flux (Neumann boundary condition). If the heat flux should be zero (homogeneous Neumann boundary condition), the boundary might be referred to as insulated wall.

If obstacles are present inside of the domain, we assume that the walls of the obstacle are insulated. This means the we apply homogeneous Neumann boundary conditions on the surface of obstacles. More about this can be found on the next pages.

Make sure that you read in a parameter ``gamma`` for the Donor-cell scheme of the heat equation.

.. note::
   - The temperature is a cell-centered quantity. Thus is is defined on the same coordinates as the pressure. 
   - We assume that there is no heat source nor sink present, i.e. :math:`q\equiv0`
   - The temperature affects the velocity of the fluid via the terms :math:`(1-\beta^*T)g_x` and :math:`(1-\beta^*T)g_y` in the momentum equations which affects the implementation of the terms :math:`\tilde{F}^{n+1}_{i,j}` and :math:`\tilde{G}^{n+1}_{i,j}`.
   - Add parameters ``gamma``, ``prandtl`` and ``beta`` to your input file parser
   
      - ``gamma`` is used for the Donor-cell scheme of the heat equation
      - ``prandtl`` is used to read the Prandtl number from the input file
      - ``beta`` is used to read the volumetric expansion coefficient from the input file
      
   - The Prandtl number :math:`Pr` affects the admissible time step size :math:`dt`.

Writing output for ParaView
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The field variable for the temperature should be called ``T`` in the output files. An updated ``output_writer_paraview.cpp`` can be found here: :download:`Download files <resources/output_writer_paraview.cpp>`

If your code is using MPI for parallelization, please update your class accordingly.

.. _ex3-heattransport-verification:

Verification
------------

As a simple test case for verification we use the lid-driven cavity test case with a heated lid. This test case has been studied extensively in literature. You can find this test case, for example, in the publication of Cheng and Liu [CL2010]_. 

.. image:: images/liddrivencavitywithheat.png
   :scale: 70 %
   :align: center

Below you find the input file that we have used. The temperature at the top is set to :math:`T_h=1` and the temperature at the bottom is set to :math:`T_c=0`. 

You can also download the input file and some the VTK-files for different times here:
:download:`Download files <resources/liddrivencavity/liddrivencavity_heat.zip>`. You will find additional test soon in the examples

**Additional sample solution**. The input file assumes that the extension for geometries has been implemented: :download:`Download files <resources/liddrivencavity/liddriven_cavity_heat_re316_ri10_sample_solution_t_5.zip>`. We have added debugging output. Please check the remarks in :ref:`ex3-geometry-verification`.

.. literalinclude:: resources/liddrivencavity/heat_without_geometryfile/liddrivencavity_heat_re316_ri10.input

.. note::
   Just for your information. When the test case is discussed, people often talk about the `Richardson number`_ and the `Grashof number`_ in order to specify the test case parameters. 

.. note:: 
   Another debugging hint:
   
   If the test case fails, you can go a step back and set the velocities on all boundaries to zero. Then set the Prandtl number to a reasonable (high) value, :math:`\beta` to zero and the final time ``endTime`` to a reasonable large number. You should get a linear temperature profile in your domain. If this works, check your terms involving :math:`\beta`. If it does not work, something goes wrong when you solve the heat equation.

Sample Results
~~~~~~~~~~~~~~

You can find plots of the solution below. On the left we plot the temperature with isolines at :math:`T=0.1,0.2,\ldots,0.9` and on the right you see the flow field with streamlines. The streamlines use 50 seed values (``resolution`` in Paraview) on the diagonal that goes from bottom left to top right. The results for :math:`t=100` are already in good agreement with the results from Cheng and Liu [CL2010]_. You see very nicely in the streamline plots how the flow field changes compared to the lid-driven cavity flow without heat transport. 

Results for :math:`t=10`

|pic1| |pic2|

.. |pic1| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t10.png
   :scale: 30 %
   
.. |pic2| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t10_streamlines.png
   :scale: 35 %   
   
Results for :math:`t=50`

|pic3| |pic4|

.. |pic3| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t50.png
   :scale: 30 %
   
.. |pic4| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t50_streamlines.png
   :scale: 35 %   

Results for :math:`t=100`

|pic5| |pic6|

.. |pic5| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t100.png
   :scale: 30 %
   
.. |pic6| image:: images/liddrivencavitywithheat_isolines_re316_ri10_t100_streamlines.png
   :scale: 35 %   

.. [CL2010] T.S. Cheng and W.-H. Liu: Effect of temperature gradient orientation on the characteristics of mixed convection flow in a lid-driven square cavity, Computers & Fluids, Volume 39, Issue 6, 2010, Pages 965-978, https://doi.org/10.1016/j.compfluid.2010.01.009.


.. _Richardson number: https://en.wikipedia.org/wiki/Richardson_number
.. _Grashof number: https://en.wikipedia.org/wiki/Grashof_number

