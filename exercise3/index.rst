==========
Exercise 3
==========

The goal of this exercise is to extend the fluid solver to account for **general geometries**, **heat transport** and additional boundary conditions.

.. note::
   If you are feeling uncomfortable working with the parallelized code from the **second** exercise or your code is wrong, you can use the serial code from the **first** exercise. This will not be any disadvantage for your group.
   
   In case you want to use the serial version of the code you have to make sure to include necessary changes from the second exercise in the serial code. 
   


.. toctree::
   :maxdepth: 1
   :caption: Next pages to read
   
   heattransport
   geometry_setup
   examples
   hints
   submission
   
