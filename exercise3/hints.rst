.. _ex3_submissionhints:

======================
Hints for submission 3
======================

.. _ex3_submissionhints_hints:

Hints
-----

Check out the pages about :ref:`ex3-heattransport` and :ref:`ex3-boundary-and-geometries`. A lot if hints are already included there. Besides that ensure that you can run the test cases described in the section :ref:`ex3_examples` as the automatic tests will be based on it.

Don't try to implement everything at once! Try extending your solver to solve the heat equations first. Afterwards extend it to account for initial conditions and general boundary conditions read from the geometry file. Implement the obstacles last. Run some tests in between to verify that your solver still produces the correct results. We have added some verification test cases you could use. What other setting could you use for testing your code? Remember: If a test succeeds, it does not mean that your code is correct. There can still be bugs. 

You can try to distribute work within your group. For example, on can start implementing the heat equation and the file parser for the geometry files at the same time. 

.. _ex3_submissionhints_reference_solution:

Reference solution
------------------

In order to verify the implementation of the heat transport you can use the lid-driven cavity test case with heated top. It is described in the :ref:`ex3-heattransport-verification` section of the :ref:`ex3-heattransport` section.

We also added some example test cases for verification where suited. 

.. _ex3_submissionhints_interpolation:

Data interpolation
------------------

In the current exercise it is crucial to have similar interpolation functions. Otherwise one has to expect some deviations in the presence of obstacles (since ``0 * nan = nan``). If your solution looks good, but the deviations are still large, please check if it could be caused by the interpolation. 

The sample solution interpolates as sketched in the figure below. 

.. figure:: images/interpolation.png
   :scale: 40 %
   :align: center
   
   Interpolation procedure of sample solution.

One could extend the interpolation function such that it expects a flag that determines what kind of interpolation should be used (barycentric, only in x-direction or only in y-direction).

In the Karman vortex street test case, there seems to be some freedom of what one sets inside the obstacle. In this case the relevant flow field itself should not be affected by this, but the interpolation when writing the vti-files is. We try to ignore these points (points 918, 919, 1019, 1020, 1021, 1121 and 1122) in the ``compare_solution`` executable. You can check the current version of the comparison script here :ref:`ex3_submission-system`.

.. _ex3_submissionhints_scenarios:

Scenarios
---------

The scenarios to test your submission will mainly be based on the test cases in the example section and the test cases explained for verification.

Running/Testing the code on the cluster with MPI
------------------------------------------------

Use the custom MPI installed in ``/scratch-nfs/maierbn/openmpi/install-3.1``. The paths you should set are shown in the sample job script in the section :ref:`ex2-cluster-script` as the standard MPI does not work with SLURM. If this is not enough try to additionally export the library path via

.. code-block:: bash

  export LD_LIBRARY_PATH=/scratch-nfs/maierbn/openmpi/install-3.1/bin:$LD_LIBRARY_PATH

Please let us know if this works for you.


