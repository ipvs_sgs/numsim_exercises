.. _introduction:

Introduction
============

Simulations improve the understanding and development in various fields in industry and research. There exist numerous software tools for such tasks: 

* General open-source solver frameworks, e.g. `FEniCS <https://fenicsproject.org/>`_ and `deal.II <https://dealii.org/>`_ for Finite Element based simulations of general partial differential equations.
* More specialized tools for specific physics phenomena, e.g., `OpenFOAM <https://openfoam.org/>`_ for computational fluid dynamics or `FEBio <https://febio.org/>`_ for bio-mechanical simulations.
* Commercial tools, e.g. `Comsol <https://www.comsol.com/>`_ for general multi-physics simulations or `LS-DYNA <http://www.lstc.com/products/ls-dyna>`_ for any dynamic processes.
* Software libraries that provide infrastructure on a lower level, e.g. the C++ math library `Eigen <http://eigen.tuxfamily.org/index.php?title=Main_Page>`_ that is optimized for instruction-level parallelism or the solver library `PETSc <https://www.mcs.anl.gov/petsc/>`_ for highly parallel simulations on distributed memory clusters.

Some of the above tools provide rather high-level functionality while others require manual scripting or programming to produce any meaningful results.
All of the tools require knowledge in different fields in order to work with them, including an understanding of physics, numerics, computer science and software engineering. 

In the development of new, efficient solver methods or implementation of new formulations for given problems, often the manual steps dominate and a piece of software has to be written that extends given code. Frequently, such code was already started by someone else and now needs to be extended for goals that weren't planned for in the initial design. Then it is highly beneficial if the previous code is well documented and has a modular architecture.

The aim of the exercises for this lecture is to learn some of the above. By writing your own simulation software, parallelizing it and adapting it to new scenarios, you get familiar with scientific software development in the context of simulation technology. In order to learn the basics, your project starts from scratch, the discretization schemes are rather easy to understand and the scenarios in two-dimensional space are more or less academic, compared to real-world applications. However, it will probably be well suited for getting a feeling for the importance of proper documentation and modular software architecture, acquiring new skills in debugging, endurance and time management and getting more used to transferring numerical solution ideas into real code.

This webpage is organized as follows. On the left, you find five sections for the five submissions of the exercises. Each submission is due after about 3 weeks time. Details can be found in the ILIAS course of the lecture. For each submission, tutorials and hints as well as some code snippets or template code are provided or discussed during the exercise. Your program that fulfills the requirement of the respective submission has to be uploaded in time and will be tested automatically.

