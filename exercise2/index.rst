
Exercise 2
===============

In this exercise, the goal is to **parallelize** the fluid solver of exercise 1 using the Message Passing Interface (MPI).

.. toctree::
   :maxdepth: 1
   :caption: Contents
   
   mpi
   hints
   cluster
   debugging_parallel
   profiling
   submission
   
