#include <mpi.h>
#include <iostream>

#include <vector>

int main (int argc, char *argv[])
{
  MPI_Init(&argc, &argv);
  
  // get own rank number and total number of ranks
  int ownRankNo = 0;
  int nRanks = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &ownRankNo);
  MPI_Comm_size(MPI_COMM_WORLD, &nRanks);
  
  // define the number of values to send and receive in this example
  const int nValuesToSend = 1e3;
  const int nValuesToReceive = nValuesToSend;
  
  // allocate the send and receive buffers
  std::vector<double> sendBuffer(nValuesToSend, ownRankNo);
  std::vector<double> receiveBuffer(nValuesToReceive);
  
  // determine other rank
  int otherRankNo = 0;
  if (ownRankNo == 0)
    otherRankNo = 1;
  
  // send to other rank and receive values from other rank
  MPI_Request sendRequest;
  MPI_Request receiveRequest;
  
  // post all nonblocking send and receive calls, the order is not relevant
  MPI_Isend(sendBuffer.data(), nValuesToSend, MPI_DOUBLE, otherRankNo, 0, MPI_COMM_WORLD, &sendRequest);
  MPI_Irecv(receiveBuffer.data(), nValuesToReceive, MPI_DOUBLE, otherRankNo, 0, MPI_COMM_WORLD, &receiveRequest);
  
  // wait for send and receive calls to complete
  MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
  MPI_Wait(&receiveRequest, MPI_STATUS_IGNORE);
  
  std::cout << "Rank " << ownRankNo << "/" << nRanks << ": The first received value is " << receiveBuffer[0] << "." << std::endl;
  
  MPI_Finalize();
}
