/** This example was written in the exercise on 6/11/2019.
 *  Compile: mpic++ example_of_exercise.cpp
 *  Run with 4 processes: mpirun -n 4 ./a.out
 * 
 */
#include <iostream>
#include <mpi.h>

#include <vector>

int main(int argc, char *argv[])
{
  MPI_Init(&argc, &argv);
  
  std::cout << "hi" << std::endl;
  
  int rankNo = 0;
  int nRanks = 0;
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rankNo);
  MPI_Comm_size(MPI_COMM_WORLD, &nRanks);
  
  std::cout << "rank " << rankNo << " / " << nRanks << std::endl;
  
  if (rankNo == 0)
  {
    std::cout << "on rank 0" << std::endl;
    
    // const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm
    std::vector<double> sendBuffer(5, 1.0);
    MPI_Send(sendBuffer.data(), 5, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
    sendBuffer[0] = 5;
    MPI_Send(sendBuffer.data(), 5, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD);
    sendBuffer[1] = 10;
    MPI_Send(sendBuffer.data(), 5, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD);
  }
  else 
  {
//    int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status * status)
    std::array<double,5> recvBuffer;
    MPI_Recv(recvBuffer.data(), 5, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    std::cout << rankNo << ": received: " << recvBuffer[0] << "," << recvBuffer[1] << "," << recvBuffer[2] << "," << recvBuffer[3] << "," << recvBuffer[4] << std::endl;
  }
  
  // const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatyp MPI_Op op, int root, MPI_Comm comm
  int sendBuffer = rankNo;
  int receiveBuffer = 0;
  MPI_Allreduce(&sendBuffer, &receiveBuffer, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  std::cout << rankNo << ": result: " << receiveBuffer << std::endl;
  
  MPI_Finalize();
  
  return 0;
}
