==========
Exercise 3
==========

The goal of this exercise is to use an existing and established simulation tool for fluid dynamics to simulate more complex scenarios. 

In the last two exercises we built our own (parallel) fluid simulation from scratch. However, in in practice one would rely on existing tools as far as possible and only implement and extend missing features. In this exercise we now want to use `OpenFOAM <https://openfoam.org/>`_ to simulate more complex scenarios and to learn how to install, configure, and use a sophisticated simulation software in general.

.. toctree::
   :maxdepth: 1
   :caption: Next pages to read
   
   openfoam
   tasks
   submission
