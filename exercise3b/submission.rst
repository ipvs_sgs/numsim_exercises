.. _ex3_submission:

=====================
Submission Exercise 3
=====================

**Deadline**: Please see the time table in `Ilias <https://ilias3.uni-stuttgart.de/>`_.

As submission for this exercise upload the project directories for the :ref:`ex3_tasks` as Zip file in Ilias. The Zip file should contain the **0**, **constant**, and **system** directory as well as a screen shot of the simulation results from ``Paraview`` for each task.


Questions for the interview ("Abnahme")
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Which things do you have to account for, when choosing a suitable domain scales for the simulation of the Karman vortex street?
* The vortex length of the flow after the step (from the task "Channel with a step") is a good metric to compare and test different simulations. How can you measure the length of the vortex accurately?
* If you had the job to test the OpenFOAM solver rigorously using a flow in a channel, how would you proceed? Which boudary conditions and channel geomentries would you use?
