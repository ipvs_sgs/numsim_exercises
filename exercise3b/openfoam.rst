.. _ex3_openfoam:

==========
OpenFOAM
==========

OpenFoam is an open-source toolbox mainly for computational fluid dynamics (CFD). It consists of a set of tools for pre- and post-processing and a multitude of numerical solvers.

Installation
------------

.. _ex3_openfoam_installation:

As the variety of OpenFOAM versions is huge, we recommend using (one of the most recent) `OpenFOAM ESI <https://www.openfoam.com/>`_ versions for reproducibility and compatibility reasons. To install OpenFOAM on your workstation follow the `instructions <https://www.openfoam.com/news/main-news/openfoam-v2306>`_ for your operating system.

After installation OpenFOAM should be installed in your ``/usr`` directory, e.g. ``/usr/lib/openfoam``. To use it properly you have to set some environment variables and extend your shell paths. This can either be done by typing

.. code-block:: bash

  source /usr/lib/openfoam/openfoam2306/etc/bashrc

each time you start a new bash session or terminal, or by adding this line to your local ``.bashrc`` in your home folder to load it automatically.

To check if OpenFOAM is ready to use, try to run

.. code-block:: bash

  simpleFoam -help

to get a usage message of the ``simpleFoam`` solver.

Usage
-----

.. _ex3_openfoam_usage:

OpenFOAM comes with several `solvers <https://www.openfoam.com/documentation/user-guide/standard-solvers.php>`_ and `utility tools <https://www.openfoam.com/documentation/user-guide/standard-utilities.php>`_. In the lecture only incompressible laminar flows, potentially with heat-transfer, are covered, thus we only need

* **blockMesh**: generating the simulation mesh as pre-processing step
* **icoFoam**: Transient solver for incompressible, laminar flow of Newtonian fluids
* **buoyantBoussinesqPimpleFoam**: Transient solver for buoyant, turbulent flow of incompressible fluids
* **paraFoam**: starting Paraview with the simulation data as post-processing step

for this exercise.

OpenFOAM simulations are configured by simple text files in a project directory. The structure is as follows.

* **0/** a folder containing the initial conditions for the simulation fields.

  * **p** the initial conditions for the pressure
  * **U** the initial conditions for the velocity
  * **T** (if required) the initial conditions for the temperature

* **constant/** a folder with simulation constants

  * **transportProperties** the fluid properties
  * **turbulenceProperties** (if required) configuration for additional turbulence models. Settings in this file can be set to ``simulationType  laminar;`` and ignored otherwise.

* **system/** a folder with solver settings and configurations

  * **blockMeshDict** a dictionary describing the geometry
  * **controlDict** a dictionary configuring the simulation
  * **fvSchemes** and **fvSolutions** the configuration of the solvers

After running the simulation the results for all saved timesteps are in separate subfolders in the project directory. They can be viewed with ``paraview`` by running

.. code-block:: bash

  paraFoam

or

.. code-block:: bash

  paraFoam -builtin

