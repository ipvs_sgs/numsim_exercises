.. _ex3_tasks:

=====
Tasks
=====

To get started with OpenFOAM we first want to reproduce the solutions of our self-written code. OpenFOAM comes with sample projects located in the installation folder.

Driven Cavity
-------------

.. _ex3_tasks_cavity:

Follow the first few steps of the tutorial `Lid-driven cavity <https://www.openfoam.com/documentation/tutorial-guide/tutorialse2.php#x6-60002.1>`_ and modify the configuration files to match our previous default simulation scenario.

.. note::
   The OpenFOAM solvers are 3D. To simulate a 2D domain, the boundary conditions and discretization in the Z domain have to be set accordingly.

Driven Cavity with heat
-----------------------

.. _ex3_tasks_cavity_with_heat:

Extend the 2D cavity tutorial to include heat transfer. The heat transfer should be induced through a temperature difference between the top and the bottom of the lid-driven cavity, the walls on the left and on the right are considered adiabatic. Simulating this scenario requires ``buoyantBoussinesqPimpleFoam`` and corresponding settings in the ``transportProperties`` file.


Channel with a Step
-------------------

.. _ex3_tasks_channel:

Modify the geometry of the 2D cavity to get a channel with a step. The channel is bounded by solid walls from the top and bottom. The left boundary above the step is the inflow and the right boundary is the outflow.

.. _step:
.. figure:: images/step.svg
  :width: 80%
  :align: center

  Geometry of the channel with a step.

.. note::
   There is a sample project with a similar setting, but for compressible flows and a more complex geometry.


Karman Vortex Street
--------------------

.. _ex3_tasks_vortex:

Modify the 2D channel to get a channel with a cylindric obstacle at the beginning.

.. _vortex:
.. figure:: images/vortex.svg
  :width: 80%
  :align: center

  Geometry of the Karman vortex street.

.. note::
   To make the cylinder you have to define the respective edges of the blocks *II*, *V*, *VI*, and *IX* as arcs.
