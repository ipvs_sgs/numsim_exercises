===============
Project
===============

So far, you had to follow the given exercises and have developed your own efficient, well-documented, clean-coded fluid solver.  Now, it's time to choose for yourself, what interesting topic should be investigated with your program. Extend your program and conduct comprehensive studies for one of the following topics. At the end, summarize your results in a talk.

Suggestions for projects
========================

- Solving of simple mechanical fluid-structure interactions (FSI). (With own dummy structure solver for example)
- Visualization of free surfaces
- Replacing the discretization by a Lattice-Boltzmann method (LBM)
- Discretization with space-time adaptive grids (Warning! This is very cool, but also very challenging.)
- Simulation of problems in 3D with efficient parallelization.
- Geometric multigrid method for the pressure equation.
- Simulations with free geometries and other boundary conditions, similar to the OpenFOAM tasks. 
- Coupling multiple simulations with preCICE: Either multiple instances of your own code or your code with OpenFOAM as an external (e.g. heat) solver. (This requires work to familiarize yourself with precise.)
- Improvement of the parallelization

   - Implementation on GPUs using CUDA or OpenCL
   - Hybrid shared/distributed memory implementation with MPI and OpenMP, Communications hiding and/or remote memory access (RMA)
  
  If you choose to improve the parallelization, you should achieve a clear speedup compared to your implementation of exercise sheet 2. The speedup has to be measurable!
  
In case you have another idea, please get in touch with us. If you choose any of the projects above you are good to go and don't have to inform us about your choice. It is completly fine if more than one group works on the same project.

Remarks regarding the project
=============================

The submission and the oral examination will be slightly different from the previous exams. 
Your submission should contain the following:

- The (working!) source code including working CMake file.
- All needed parameters and configuration files
- All further information needed to run your code.
- All details of the visualization and further post-processing routines

The points above should ensure that you have as much freedom as possible when coding with us having the chance to understand the capabilies of your code. Otherwise a fair evaluation is not possible.

You can submit your project in Ilias.

You should prepare a 10 minute presentation for your final examination. It should discuss how the code has been extended and improved. This includes the presentation of your results and a summary/conclusion of your work. Make sure that it is clear what each group members contributed to the project. The presentation should not cover much about the basics since this was part of the lecture, but rather focus on your work. 


Hints for preCICE
=================

There is an old exercise on coupling different solvers with preCICE with hints on how to use and compile preCICE.

.. toctree::
   :maxdepth: 1
   
   ../exercise4/index

